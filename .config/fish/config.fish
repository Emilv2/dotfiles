if status is-interactive
    # Commands to run in interactive sessions can go here
end

# function fish_prompt
#     string join '' -- (set_color green) (prompt_pwd) (set_color normal) '>'
# end
#
set -g __fish_git_prompt_show_informative_status 1
set -g __fish_git_prompt_showcolorhints 1
set -g __fish_git_prompt_showupstream verbose

function fish_hybrid_key_bindings --description \
"Vi-style bindings that inherit emacs-style bindings in all modes"
    for mode in default insert visual
        fish_default_key_bindings -M $mode
    end
    fish_vi_key_bindings --no-erase
end
set -g fish_key_bindings fish_hybrid_key_bindings

function short_pwd --description \
"shorten pwd to minimal unique length, but keep git root directories full length"
    set -l -- cur_dir (path basename $PWD)
    set -l -- cur_path (path dirname $PWD)
    set -l -- shortened_path

    if [ "$cur_dir" = '/' ]
        echo "$cur_dir"
        return
    end

    if [ (path filter $cur_path/$cur_dir/'.git') ]
        set -p -- shortened_path (string join -- '' (set_color red) $cur_dir (set_color normal))
    else
        set -p -- shortened_path (string join -- '' (set_color green) $cur_dir (set_color normal))
    end

    set -l -- cur_dir (path basename $cur_path)
    set -l -- cur_path (path dirname $cur_path)

    set -l -- i 1
    while [ "$cur_dir" != '/' ]
        set suffix ''

        if [ "$cur_path"/"$cur_dir" = "$HOME" ]
            set -- cur_dir '~'
            break
        end

        if [ (path filter $cur_path/$cur_dir/'.git') ]
            set -p -- shortened_path (string join -- '' (set_color red) $cur_dir (set_color normal))

        else
            for i in (seq 1 (string length $cur_dir))
                # no need to shorten if we are at length -1 since we're adding a character anyway
                if [ "$i" = (math (string length $cur_dir) - 1) ]
                    set -- i (string length $cur_dir)
                    break
                else if [ (count (path filter --type dir -- $cur_path/(string sub --length $i -- $cur_dir)*)) = 1 ]
                    set -- suffix (string join -- '' (set_color cyan) '*' (set_color normal))
                    break
                end

            end

            set -p -- shortened_path (string join -- '' (set_color green) (string sub --length $i -- $cur_dir)$suffix (set_color normal))
        end

        set -- i 1
        set -- cur_dir (path basename -- $cur_path)
        set -- cur_path (path dirname -- $cur_path)


    end

    set -p -- shortened_path (string trim --chars=/ -- $cur_dir)

    string join -- '/' $shortened_path
end


function fish_prompt --description 'Write out the prompt'
    set -l last_pipestatus $pipestatus
    set -lx __fish_last_status $status # Export for __fish_print_pipestatus.
    set -l normal (set_color normal)
    set -q fish_color_status
    or set -g fish_color_status red

    # Color the prompt differently when we're root
    set -l color_cwd $fish_color_cwd
    set -l suffix '>'
    if functions -q fish_is_root_user; and fish_is_root_user
        if set -q fish_color_cwd_root
            set color_cwd $fish_color_cwd_root
        end
        set suffix '#'
    end

    # Write pipestatus
    # If the status was carried over (if no command is issued or if `set` leaves the status untouched), don't bold it.
    set -l bold_flag --bold
    set -q __fish_prompt_status_generation; or set -g __fish_prompt_status_generation $status_generation
    if test $__fish_prompt_status_generation = $status_generation
        set bold_flag
    end
    set __fish_prompt_status_generation $status_generation
    set -l status_color (set_color $fish_color_status)
    set -l statusb_color (set_color $bold_flag $fish_color_status)
    set -l prompt_status (__fish_print_pipestatus "[" "]" "|" "$status_color" "$statusb_color" $last_pipestatus)

    echo -n -s (prompt_login)' ' (set_color $color_cwd) (short_pwd) $normal (fish_vcs_prompt) $normal " "$prompt_status $suffix " "
end
