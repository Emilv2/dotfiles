let mapleader = " "

""" copy to and paste from system clipboard
noremap <leader>p "+p
noremap <leader>P "+P
noremap <leader>y "+Y

nnoremap <leader>t :NERDTreeToggle<cr>
nnoremap <leader>b :buffers<cr>:b<space>

" to use python while in virtualenv
let g:python3_host_prog = '/usr/bin/python3'

set number
set splitright
" don't ask to save buffers on switch
set hidden

""" don't do automatic colorchanging
""" solarized colors do fine on light and dark background
set background=light

" vim-plug https://github.com/junegunn/vim-plug
call plug#begin('~/.local/share/nvim/plugged')

" NERDTree sidebar filemanager
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'itchyny/lightline.vim'
Plug 'junegunn/fzf.vim'
""" show git diff with +,- in the sign column (left of number column)
Plug 'airblade/vim-gitgutter'
""" keep the sign column background the same as the rest
highlight SignColumn guibg=none ctermbg=none
""" always show the sign columns so it doesn't toggle all the time
set signcolumn=yes


" lsp for autocomplete etc
Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }


Plug 'Xuyuanp/nerdtree-git-plugin', { 'on':  'NERDTreeToggle' }

Plug 'lervag/vimtex'
let g:tex_flavor = 'latex'

let g:python3_host_prog = '/usr/bin/python3'

Plug 'ntpeters/vim-better-whitespace'

" vhdl tools
Plug 'scrooloose/syntastic'
Plug 'http://git.vhdltool.com/vhdl-tool/syntastic-vhdl-tool'

""" Jedi for python
""" needs pynvim and jedi python packages
Plug 'zchee/deoplete-jedi'

""" comment and uncomment lines
Plug 'tomtom/tcomment_vim'

call plug#end()

let g:better_whitespace_enabled=1
let g:strip_whitespace_on_save=1

""" start deoplete configuration
let g:deoplete#enable_at_startup = 1

inoremap <silent><expr> <TAB>
    \ pumvisible() ? "\<C-n>" :
    \ <SID>check_back_space() ? "\<TAB>" :
    \ deoplete#mappings#manual_complete()
function! s:check_back_space() abort "{{{
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
endfunction"}}}

" close help window after autocomplete
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

""" end deoplete configuration


let g:LanguageClient_serverCommands = {
    \ 'rust': ['~/.cargo/bin/rustup', 'run', 'stable', 'rls'],
    \ 'javascript': ['/usr/local/bin/javascript-typescript-stdio'],
    \ 'javascript.jsx': ['tcp://127.0.0.1:2089'],
    \ 'python': ['/usr/bin/pyls'],
    \ 'vhdl': ['~/code/rust_hdl/target/release/vhdl_ls'],
    \ }

highlight GitGutterAdd    guifg=#009900 guibg=#073642 ctermfg=2
highlight GitGutterChange guifg=#bbbb00 guibg=#073642 ctermfg=3
highlight GitGutterDelete guifg=#ff2222 guibg=#073642 ctermfg=1

nnoremap <F5> :call LanguageClient_contextMenu()<CR>
nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>

""" create a jump list
""" https://vim.fandom.com/wiki/Jumping_to_previously_visited_locations
""" CC BY-SA 3.0
function! GotoJump()
  jumps
  let j = input("Please select your jump: ")
  if j != ''
    let pattern = '\v\c^\+'
    if j =~ pattern
      let j = substitute(j, pattern, '', 'g')
      execute "normal " . j . "\<c-i>"
    else
      execute "normal " . j . "\<c-o>"
    endif
  endif
endfunction
nmap <Leader>j :call GotoJump()<CR>

""" diff current buffer with file on disk
""" https://stackoverflow.com/questions/6426154/taking-a-quick-look-at-difforig-then-switching-back
""" CC BY-SA 3.0
command DiffOrig let g:diffline = line('.') | vert new | set bt=nofile | r # | 0d_ | diffthis | :exe "norm! ".g:diffline."G" | wincmd p | diffthis | wincmd p
nnoremap <Leader>do :DiffOrig<cr>
nnoremap <leader>dc :q<cr>:diffoff<cr>:exe "norm! ".g:diffline."G"<cr>

""" easily edit files in same folder with :e %%/file
""" https://vim.fandom.com/wiki/Easy_edit_of_files_in_the_same_directory
cabbr <expr> %% expand('%:p:h')

""" fix highlighting colors in both dark and light themes
hi Search ctermbg=DarkMagenta
hi Search ctermfg=White
hi Visual ctermbg=Gray
hi Visual ctermfg=White
